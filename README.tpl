# Nginx Docker image running on Alpine Linux

[![Docker Automated build](https://img.shields.io/docker/automated/maurosoft1973/alpine-nginx.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/maurosoft1973/alpine-nginx/)
[![Docker Pulls](https://img.shields.io/docker/pulls/maurosoft1973/alpine-nginx.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/maurosoft1973/alpine-nginx/)
[![Docker Stars](https://img.shields.io/docker/stars/maurosoft1973/alpine-nginx.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/maurosoft1973/alpine-nginx/)

[![Alpine Version](https://img.shields.io/badge/Alpine%20version-v%ALPINE_VERSION%-green.svg?style=for-the-badge)](https://alpinelinux.org/)
[![Nginx Version](https://img.shields.io/docker/v/maurosoft1973/alpine-nginx?sort=semver&style=for-the-badge)](https://nginx.org/)


This Docker image [(maurosoft1973/alpine-nginx)](https://hub.docker.com/r/maurosoft1973/alpine-nginx/) is based on the minimal [Alpine Linux](https://alpinelinux.org/) with [Nginx v%NGINX_VERSION%](https://nginx.org/) web server.

##### Alpine Version %ALPINE_VERSION% (Released %ALPINE_VERSION_DATE%)
##### Nginx Version %NGINX_VERSION% (Released %NGINX_VERSION_DATE%)

----

## What is Alpine Linux?
Alpine Linux is a Linux distribution built around musl libc and BusyBox. The image is only 5 MB in size and has access to a package repository that is much more complete than other BusyBox based images. This makes Alpine Linux a great image base for utilities and even production applications. Read more about Alpine Linux here and you can see how their mantra fits in right at home with Docker images.

## What is Nginx?
NGINX is open source software for web serving, reverse proxying, caching, load balancing, media streaming, and more. It started out as a web server designed for maximum performance and stability. In addition to its HTTP server capabilities, NGINX can also function as a proxy server for email (IMAP, POP3, and SMTP) and a reverse proxy and load balancer for HTTP, TCP, and UDP servers.

The goal behind NGINX was to create the fastest web server around, and maintaining that excellence is still a central goal of the project. NGINX consistently beats Apache and other servers in benchmarks measuring web server performance. Since the original release of NGINX, however, websites have expanded from simple HTML pages to dynamic, multifaceted content. NGINX has grown along with it and now supports all the components of the modern Web, including WebSocket, HTTP/2, gRPC, and streaming of multiple video formats (HDS, HLS, RTMP, and others).

The versions of images are:
* base : only nginx web server
* geo : nginx web server + maxmind geo module with database

## Architectures

* ```:aarch64``` - 64 bit ARM
* ```:armhf```   - 32 bit ARM v6
* ```:armv7```   - 32 bit ARM v7
* ```:ppc64le``` - 64 bit PowerPC
* ```:x86```     - 32 bit Intel/AMD
* ```:x86_64```  - 64 bit Intel/AMD (x86_64/amd64)

## Tags

* ```:latest```             latest branch based (Automatic Architecture Selection)
* ```:aarch64```            latest 64 bit ARM
* ```:armhf```              latest 32 bit ARM v6
* ```:armv7```              latest 32 bit ARM v7
* ```:ppc64le```            latest 64 bit PowerPC
* ```:x86```                latest 32 bit Intel/AMD
* ```:x86_64```             latest 64 bit Intel/AMD
* ```:geo-latest```         geo latest branch based (Automatic Architecture Selection)
* ```:geo-aarch64```        geo latest 64 bit ARM
* ```:geo-armhf```          geo latest 32 bit ARM v6
* ```:geo-armv7```          geo latest 32 bit ARM v7
* ```:geo-ppc64le```        geo latest 64 bit PowerPC
* ```:geo-x86```            geo latest 32 bit Intel/AMD
* ```:geo-x86_64```         geo latest 64 bit Intel/AMD
* ```:test```               test branch based (Automatic Architecture Selection)
* ```:test-aarch64```       test 64 bit ARM
* ```:test-armhf```         test 32 bit ARM v6
* ```:test-armv7```         test 32 bit ARM v7
* ```:test-ppc64le```       test 64 bit PowerPC
* ```:test-x86```           test 32 bit Intel/AMD
* ```:test-x86_64```        test 64 bit Intel/AMD
* ```:geo-test```           geo test branch based (Automatic Architecture Selection)
* ```:geo-test-aarch64```   geo test 64 bit ARM
* ```:geo-test-armhf```     geo test 32 bit ARM v6
* ```:geo-test-armv7```     geo test 32 bit ARM v7
* ```:geo-test-ppc64le```   geo test 64 bit PowerPC
* ```:geo-test-x86```       geo test 32 bit Intel/AMD
* ```:geo-test-x86_64```    geo test 64 bit Intel/AMD
* ```:%ALPINE_VERSION%```           %ALPINE_VERSION% branch based (Automatic Architecture Selection)
* ```:%ALPINE_VERSION%-aarch64```   %ALPINE_VERSION% 64 bit ARM
* ```:%ALPINE_VERSION%-armhf```     %ALPINE_VERSION% 32 bit ARM v6
* ```:%ALPINE_VERSION%-armv7```     %ALPINE_VERSION% 32 bit ARM v7
* ```:%ALPINE_VERSION%-ppc64le```   %ALPINE_VERSION% 64 bit PowerPC
* ```:%ALPINE_VERSION%-x86```       %ALPINE_VERSION% 32 bit Intel/AMD
* ```:%ALPINE_VERSION%-x86_64```    %ALPINE_VERSION% 64 bit Intel/AMD
* ```:%ALPINE_VERSION%-%NGINX_VERSION%```           %ALPINE_VERSION%-%NGINX_VERSION% branch based (Automatic Architecture Selection)
* ```:%ALPINE_VERSION%-%NGINX_VERSION%-aarch64```   %ALPINE_VERSION%-%NGINX_VERSION% 64 bit ARM
* ```:%ALPINE_VERSION%-%NGINX_VERSION%-armhf```     %ALPINE_VERSION%-%NGINX_VERSION% 32 bit ARM v6
* ```:%ALPINE_VERSION%-%NGINX_VERSION%-armv7```     %ALPINE_VERSION%-%NGINX_VERSION% 32 bit ARM v7
* ```:%ALPINE_VERSION%-%NGINX_VERSION%-ppc64le```   %ALPINE_VERSION%-%NGINX_VERSION% 64 bit PowerPC
* ```:%ALPINE_VERSION%-%NGINX_VERSION%-x86```       %ALPINE_VERSION%-%NGINX_VERSION% 32 bit Intel/AMD
* ```:%ALPINE_VERSION%-%NGINX_VERSION%-x86_64```    %ALPINE_VERSION%-%NGINX_VERSION% 64 bit Intel/AMD
* ```:geo-%ALPINE_VERSION%```           %ALPINE_VERSION% branch based (Automatic Architecture Selection)
* ```:geo-%ALPINE_VERSION%-aarch64```   %ALPINE_VERSION% 64 bit ARM
* ```:geo-%ALPINE_VERSION%-armhf```     %ALPINE_VERSION% 32 bit ARM v6
* ```:geo-%ALPINE_VERSION%-armv7```     %ALPINE_VERSION% 32 bit ARM v7
* ```:geo-%ALPINE_VERSION%-ppc64le```   %ALPINE_VERSION% 64 bit PowerPC
* ```:geo-%ALPINE_VERSION%-x86```       %ALPINE_VERSION% 32 bit Intel/AMD
* ```:geo-%ALPINE_VERSION%-x86_64```    %ALPINE_VERSION% 64 bit Intel/AMD
* ```:geo-%ALPINE_VERSION%-%NGINX_VERSION%```           geo-%ALPINE_VERSION%-%NGINX_VERSION% branch based (Automatic Architecture Selection)
* ```:geo-%ALPINE_VERSION%-%NGINX_VERSION%-aarch64```   geo-%ALPINE_VERSION%-%NGINX_VERSION% 64 bit ARM
* ```:geo-%ALPINE_VERSION%-%NGINX_VERSION%-armhf```     geo-%ALPINE_VERSION%-%NGINX_VERSION% 32 bit ARM v6
* ```:geo-%ALPINE_VERSION%-%NGINX_VERSION%-armv7```     geo-%ALPINE_VERSION%-%NGINX_VERSION% 32 bit ARM v7
* ```:geo-%ALPINE_VERSION%-%NGINX_VERSION%-ppc64le```   geo-%ALPINE_VERSION%-%NGINX_VERSION% 64 bit PowerPC
* ```:geo-%ALPINE_VERSION%-%NGINX_VERSION%-x86```       geo-%ALPINE_VERSION%-%NGINX_VERSION% 32 bit Intel/AMD
* ```:geo-%ALPINE_VERSION%-%NGINX_VERSION%-x86_64```    geo-%ALPINE_VERSION%-%NGINX_VERSION% 64 bit Intel/AMD
## Layers & Sizes

| Version                                                                               | Size                                                                                                                 |
|---------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------|
| ![Version](https://img.shields.io/badge/version-amd64-blue.svg?style=for-the-badge)   | ![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/alpine-nginx/latest?style=for-the-badge)  |
| ![Version](https://img.shields.io/badge/version-armv6-blue.svg?style=for-the-badge)   | ![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/alpine-nginx/armhf?style=for-the-badge)   |
| ![Version](https://img.shields.io/badge/version-armv7-blue.svg?style=for-the-badge)   | ![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/alpine-nginx/armv7?style=for-the-badge)   |
| ![Version](https://img.shields.io/badge/version-ppc64le-blue.svg?style=for-the-badge) | ![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/alpine-nginx/ppc64le?style=for-the-badge) |
| ![Version](https://img.shields.io/badge/version-x86-blue.svg?style=for-the-badge)     | ![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/alpine-nginx/x86?style=for-the-badge)     |


## Volume structure

* `/var/www`:
* `/etc/nginx/sites-enabled`: server block configuration file

## Environment Variables:

### Main Nginx parameters:
* `LC_ALL`: default locale (en_GB.UTF-8)
* `TIMEZONE`: default timezone (Europe/Brussels)
* `WWW_USER`: specify the user ownership (default www)
* `WWW_USER_ID`: specify the user identifier (uid) associate at WWW_USER (default 5001)
* `WWW_GROUP`: specify the group ownership (default www-data)
* `WWW_GROUP_ID`: specify the user group identifier (gid) associate as WWW_GROUP (default 33)

***
###### Last Update %LAST_UPDATE%
